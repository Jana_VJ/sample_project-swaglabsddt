package com.ddt.testcases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ddt.testbase.BaseClass;

public class EndToEndTest extends BaseClass {

	@Test
	public void execute() {
		
		try {

		lp.enter_username("standard_user");

		lp.enter_password("secret_sauce");

		lp.click_login();

		hp.add_product("Sauce Labs Backpack");

		hp.check_cart();
		
//		throw new Error(new ArrayIndexOutOfBoundsException());

		cp.click_checkout();

		cop.enter_firtName("As");

		cop.enter_lastName("As");

		cop.enter_postalCode("AS-01");

		cop.click_continue();
		
		pp.click_finish();

		Assert.assertTrue(confirmpage.check_confirmation_logo());

		confirmpage.click_home_button();

		Assert.assertTrue(hp.verify_logo());
		
		hp.logout();
		
		}
		catch(Exception e) {
			
			e.printStackTrace();
			
			System.out.println(e.getMessage());
		}

	}

}
