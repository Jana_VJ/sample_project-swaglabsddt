package com.ddt.testcases;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.ddt.testbase.BaseClass;

public class HomePageTest extends BaseClass{
	
	@Test()
	@Parameters({"username", "password"})
	public void valid_login_two(String username, String password) {
		
		lp.enter_username(username);
		
		lp.enter_password(password);
		
		lp.click_login();
	}
	

}
