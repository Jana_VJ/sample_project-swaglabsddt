package com.ddt.testcases;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.ddt.pageobjects.LoginPage;
import com.ddt.testbase.BaseClass;

public class LoginPageTest extends BaseClass {
	
	@Test(priority = 0)
	public void verify_logo() {
		
		Assert.assertTrue(lp.checkLogo());
	}

	@Test(priority = 1)
	public void validate_login() {
		
		String username = prop.getProperty("username");
		
		lp.enter_username(username);
		
		String password = prop.getProperty("password");
		
		lp.enter_password(password);
		
		lp.click_login();
		
	}
	
	



}
