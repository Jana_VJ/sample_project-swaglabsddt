package com.ddt.testcases;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.ddt.testbase.BaseClass;

public class TestClass extends BaseClass{
	
	@Test
	public void test() throws InterruptedException {
		
		lp.default_Login();
		
		List<WebElement> product_names = driver.findElements(By.xpath("//div[@class='inventory_item_name ']"));
		
		System.out.println("Total Products : "+product_names.size());
		
		for(WebElement product_name : product_names) {
			
			System.out.println(product_name.getText());
			
		}
		
//		hp.logout();
//		
//		Assert.assertTrue(lp.checkLogo());
		
		hp.add_product("Sauce Labs Onesie");
		
		Thread.sleep(5000);
		
		hp.check_cart();
		
		Thread.sleep(5000);
		
		cp.click_checkout();
		
		Thread.sleep(5000);
	}

}
