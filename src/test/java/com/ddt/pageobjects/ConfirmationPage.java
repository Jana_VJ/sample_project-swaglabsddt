package com.ddt.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ConfirmationPage extends BasePage {

	public ConfirmationPage(WebDriver driver) {

		super(driver);
	}

	// Locators

	@FindBy(xpath = "//span[@class='title']")
	private WebElement page_title;

	@FindBy(xpath = "//img[@alt='Pony Express']")
	private WebElement img_confirm_logo;

	@FindBy(xpath = "//div[@id='checkout_complete_container']/h2")
	private WebElement txt_confirmation_msg;

	@FindBy(xpath = "//button[@id='back-to-products']")
	private WebElement btn_back_home;

	// Actions
	
	public boolean check_confirmation_logo() {
		
		return img_confirm_logo.isDisplayed();
	}
	
	public void click_home_button() {
		
		btn_back_home.click();
	}

}
