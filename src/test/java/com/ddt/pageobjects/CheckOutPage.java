package com.ddt.pageobjects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CheckOutPage extends BasePage {

	public CheckOutPage(WebDriver driver) {

		super(driver);
	}

	@FindBy(xpath = "//span[@class='title']")
	private WebElement page_title;

	@FindBy(id = "first-name")
	private WebElement txt_firstName;

	@FindBy(id = "last-name")
	private WebElement txt_lastName;

	@FindBy(id = "postal-code")
	private WebElement txt_postalCode;

	@FindBy(css = "div.error-message-container.error h3")
	private WebElement txt_error_message;

	@FindBy(xpath = "//div[contains(@class,'error')]/h3/button")
	private WebElement btn_error_close;

	@FindBy(xpath = "//div[@class='checkout_buttons']/button")
	private WebElement btn_cancel;

	@FindBy(id = "continue")
	private WebElement btn_continue;

	
	// Actions

	public void enter_firtName(String firstName) {

		txt_firstName.sendKeys(firstName);
	}

	public void enter_lastName(String lastName) {

		txt_lastName.sendKeys(lastName);
	}

	public void enter_postalCode(String postalCode) {

		txt_postalCode.sendKeys(postalCode);
	}
	
	public void click_cancel() {
		
		btn_cancel.click();
	}
	
	public void click_continue() {
		
		btn_continue.click();
	}

}
