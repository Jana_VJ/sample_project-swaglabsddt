package com.ddt.pageobjects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CartPage extends BasePage{

	public CartPage(WebDriver driver) {
		
		super(driver);
	}
	// Locators 
	
		@FindBy(xpath = "//span[@class='title']")
		private WebElement page_title;
		
		@FindBy(xpath = "//div[@class='cart_item']")
		private List<WebElement> total_cart_items;
		
		@FindBy(xpath = "//div[@class='inventory_item_name']")
		private List<WebElement> cart_product_name;
		
		@FindBy(xpath = "//div[@class='inventory_item_price']")
		private WebElement cart_product_prize;
		
		@FindBy(xpath = "//div[@class='item_pricebar']/button")
		private WebElement btn_remove_product;
		
		@FindBy(id = "continue-shopping")
		private WebElement btn_continue_shopping;
		
		@FindBy(xpath = "//button[@id='checkout']")
		private WebElement btn_checkout;
		
		
		// Actions
		
		public void click_checkout() {
			
			btn_checkout.click();
		}
		
		public void click_continue_shopping() {
			
			btn_continue_shopping.click();
		}
		
		
}
