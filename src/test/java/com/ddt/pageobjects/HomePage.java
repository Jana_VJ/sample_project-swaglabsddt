package com.ddt.pageobjects;

import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

	public HomePage(WebDriver driver) {
		super(driver);
	}

	// Locators

	@FindBy(css = "div.app_logo")
	private WebElement app_logo;

	@FindBy(id = "react-burger-menu-btn")
	private WebElement btn_menu;

	@FindBy(id = "react-burger-cross-btn")
	private WebElement btn_menu_close;

	@FindBy(id = "inventory_sidebar_link")
	private WebElement txt_allItems;

	@FindBy(id = "about_sidebar_link")
	private WebElement txt_about;

	@FindBy(id = "logout_sidebar_link")
	private WebElement txt_logout;

	@FindBy(id = "reset_sidebar_link")
	private WebElement txt_reset;

	@FindBy(id = "shopping_cart_container")
	private WebElement icon_cart;

	@FindBy(xpath = "//div[@class='pricebar']/button")
	private List<WebElement> btn_addToCart;

	@FindBy(xpath = "//span[@class='title']")
	private WebElement page_title;

	@FindBy(xpath = "inventory_item")
	private List<WebElement> total_products;

	@FindBy(xpath = "//div[@class='inventory_item_name ']")
	private List<WebElement> product_Name;

	@FindBy(xpath = "//div[@class='inventory_item_price']")
	private List<WebElement> product_prize;

	@FindBy(xpath = "//div[@class='right_component']/span/span")
	private WebElement dropdown_filter;

	@FindBy(xpath = "//footer[@class='footer']/ul/li/a")
	private List<WebElement> icon_social;

	// Actions
	
	public boolean verify_logo() {
		
		return app_logo.isDisplayed();
	}

	public void logout() {

		btn_menu.click();

		txt_logout.click();
	}

	public void add_products(List<String> products) {

	}

	public void add_product(String product) {

		for (int i = 0; i < product_Name.size(); i++) {

			String prod = product_Name.get(i).getText();

			if (product.equalsIgnoreCase(prod)) {

				btn_addToCart.get(i).click();

				break;
			}
		}
	}

	public void check_cart() {

		icon_cart.click();
	}

	public String verify_pageTitle() {

		return page_title.getText();
	}

}
