package com.ddt.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {

	public WebDriver driver;

	public LoginPage(WebDriver driver) {

		super(driver);

	}

	// Locators
	
	@FindBy(css="div.login_logo")
	private WebElement app_logo;

	@FindBy(id = "user-name")
	private WebElement txt_username;

	@FindBy(id = "password")
	private WebElement txt_password;

	@FindBy(id = "login-button")
	private WebElement btn_login;

	@FindBy(css = "div.error-message-container.error")
	private WebElement error_msg;

	@FindBy(css = "button.error-button")
	private WebElement error_close;

	
	
	
	// Action Methods
	
	public boolean checkLogo() {
		
		return app_logo.isDisplayed(); 
	}
	
	public void enter_username(String username) {
		
		txt_username.sendKeys(username);
	}
	
	public void enter_password(String password) {
		
		txt_password.sendKeys(password);
	}
	
	public HomePage click_login() {
		
		btn_login.click();
		
		return new HomePage(driver);
	}
	
	public String get_error_message() {
		
		return error_msg.getText();
	}
	
	public void close_error() {
		
		error_close.click();
	}
	
	public void clear_input_fields() {
		
		txt_username.clear();
		
		txt_password.clear();
	}
	
	public void default_Login() {
		
		txt_username.sendKeys("standard_user");
		
		txt_password.sendKeys("secret_sauce");
		
		btn_login.click();
	}
	
}
