package com.ddt.pageobjects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PaymentPage extends BasePage{

	public PaymentPage(WebDriver driver) {
		
		super(driver);
	}
	
	
	// Locators
	
	@FindBy(xpath = "//span[@class='title']")
	private WebElement page_title;
	
	@FindBy(xpath = "//div[@class='cart_item']")
	private WebElement checkout_total_products;
	
	@FindBy(xpath = "//div[@class='cart_item_label']/a")
	private List<WebElement> checkout_product_name;
	
	@FindBy(xpath = "//div[@class='inventory_item_price']")
	private List<WebElement> checkout_product_prize;
	
	@FindBy(xpath = "//div[@class='summary_subtotal_label']")
	private WebElement checkout_subtotal_amount;
	
	@FindBy(xpath = "//div[@class='summary_tax_label']")
	private WebElement checkout_tax_amount;	
	
	@FindBy(css = "div.summary_info_label.summary_total_label")
	private WebElement checkout_total_amount;
	
	@FindBy(xpath = "//button[@id='cancel']")
	private WebElement btn_cancel;
	
	@FindBy(xpath = "//button[@id='finish']")
	private WebElement btn_finish;
	
	
	// Actions
	
	public void click_finish() {
		
		btn_finish.click();
	}
	
	

}
