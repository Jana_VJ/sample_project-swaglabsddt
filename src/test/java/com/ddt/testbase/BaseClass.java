package com.ddt.testbase;

import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.ddt.pageobjects.CartPage;
import com.ddt.pageobjects.CheckOutPage;
import com.ddt.pageobjects.ConfirmationPage;
import com.ddt.pageobjects.HomePage;
import com.ddt.pageobjects.LoginPage;
import com.ddt.pageobjects.PaymentPage;
import com.ddt.utilities.Constants;

public class BaseClass {

	public WebDriver driver;

	public Properties prop;

	public LoginPage lp;

	public HomePage hp;

	public CartPage cp;
	
	public CheckOutPage cop;
	
	public PaymentPage pp;
	
	public ConfirmationPage confirmpage;

	@BeforeMethod()
	public void loadPages() {

		lp = new LoginPage(driver);

		hp = new HomePage(driver);

		cp = new CartPage(driver);
		
		cop = new CheckOutPage(driver);
		
		pp = new PaymentPage(driver);
		
		confirmpage = new ConfirmationPage(driver);
		
		
	}

	@Parameters("browser")
	@BeforeClass
	public void setUp(@Optional String br) throws IOException {

		FileInputStream fis = new FileInputStream(Constants.configPath);

		prop = new Properties();

		prop.load(fis);

		String browser = prop.getProperty("browser") != null ? prop.getProperty("browser").toUpperCase()
				: br.toUpperCase();

		String url = prop.getProperty("url");

		if (browser.contains(Constants.chrome)) {

			ChromeOptions options = new ChromeOptions();

			System.setProperty(Constants.chromeKey, Constants.chromePath);

			if (browser.contains("HEADLESS")) {

				options.addArguments("--headless");
			}

			driver = new ChromeDriver(options);

		} else if (browser.equalsIgnoreCase(Constants.firefox)) {

			System.setProperty(Constants.fireKey, Constants.firePath);

			driver = new FirefoxDriver();

		} else if (browser.equalsIgnoreCase(Constants.edge)) {

			System.setProperty(Constants.edgeKey, Constants.edgePath);

			driver = new EdgeDriver();

		}

		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));

		driver.manage().deleteAllCookies();

		driver.get(url);

	}

	@AfterClass
	public void tearDown() {

		driver.quit();
	}

}
