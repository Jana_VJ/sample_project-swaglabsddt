package com.ddt.utilities;

public class Constants {
	
	public static final String chrome = "CHROME";
	
	public static final String firefox = "FIREFOX";
	
	public static final String edge = "EDGE";
	
	public static final String chromeKey = "webdriver.chrome.driver";
	
	public static final String fireKey = "webdriver.firefox.driver";
	
	public static final String edgeKey = "webdriver.edge.driver";
	
	public static final String chromePath = System.getProperty("user.dir")+"\\drivers\\chromedriver.exe";
	
	public static final String firePath = System.getProperty("user.dir")+"\\drivers\\geckodriver.exe";
	
	public static final String edgePath = System.getProperty("user.dir")+"\\drivers\\msedgedriver.exe";
	
	public static final String configPath = System.getProperty("user.dir")+"\\src\\test\\java\\com\\ddt\\utilities\\config.properties";
	
	

}
